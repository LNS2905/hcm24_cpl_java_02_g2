package com.fams.fams.controllers;

import com.fams.fams.models.entities.EmailSend;
import com.fams.fams.models.entities.EmailSendStudent;
import com.fams.fams.models.entities.Student;
import com.fams.fams.models.exception.FamsApiException;
import com.fams.fams.repositories.*;
import com.fams.fams.services.EmailSendService;
import com.fams.fams.utils.Template;
import jakarta.mail.MessagingException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/email-send")
public class EmailSendController {
    public EmailSendService emailSendService;
    private ClassRepository classRepository;
    private StudentRepository studentRepository;
    private EmailSendRepository emailSendRepository;
    private UserRepository userRepository;
    private EmailSendStudentRepository emailSendStudentRepository;

    public EmailSendController(EmailSendService emailSendService, ClassRepository classRepository, StudentRepository studentRepository, EmailSendRepository emailSendRepository, UserRepository userRepository, EmailSendStudentRepository emailSendStudentRepository) {
        this.emailSendService = emailSendService;
        this.classRepository = classRepository;
        this.studentRepository = studentRepository;
        this.emailSendRepository = emailSendRepository;
        this.userRepository = userRepository;
        this.emailSendStudentRepository = emailSendStudentRepository;
    }
//
//    @PostMapping("/send-trainer")
//    public ResponseEntity<?> sendEmail(@RequestParam List<Long> to,
//                                       @RequestParam String subject,
//                                       @RequestParam Long emailTemplateId,
//                                       @RequestParam Long userId) throws MessagingException {
//
//        emailSendService.sendTrainers(to, subject, emailTemplateId, userId);
//        return ResponseEntity.ok("Email sent!");
//
//    }
//
//    @PostMapping("/remind-reservation")
//    public ResponseEntity<?> informReservation(@RequestParam List<Long> ids,
//                                               @RequestParam Long trainerId) throws MessagingException, jakarta.mail.MessagingException {
//
//        if (userRepository.findWithId(trainerId) == null)
//            throw new FamsApiException(HttpStatus.NOT_FOUND, "TrainerId not found" + trainerId);
//        else {
//            List<Student> studentList = new ArrayList<>();
//            List<Long> invalidList = new ArrayList<>();
//            ids.forEach(s -> {
//                var temp = studentRepository.findWithId(s);
//                if (temp != null) studentList.add(temp);
//                else invalidList.add(s);
//            });
//            Template template = new Template();
//            String message;
//            if (!invalidList.isEmpty())
//                throw new FamsApiException(HttpStatus.NOT_FOUND, "StudentId not found " + invalidList);
//            else {
//                studentList.forEach(s1 ->
//
//                        {
//                            try {
//                                String s = "Fresher Academy miss you" + s1.getFullName();
//                                emailSendService.informReservation(s1.getEmail(),
//                                        s,
//                                        template.reservationStatus(s1.getFullName(), userRepository.findWithId(trainerId).getFullName(),
//                                                userRepository.findWithId(trainerId).getPhone()));
//                                EmailSend emailSend = new EmailSend();
//                                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//                                LocalDate now = LocalDate.now();
//                                emailSend.setSendDate(now);
//                                emailSend.setContent(s);
//                                emailSend.setUsers(userRepository.findWithId(trainerId));
//                                emailSend.setReceiverType("emailSendStudent");
//                                var emailSendResult = emailSendRepository.save(emailSend);
//                                EmailSendStudent emailSendStudent = new EmailSendStudent();
//                                emailSendStudent.setStudents(s1);
//                                emailSendStudent.setEmailSends(emailSendResult);
//                                emailSendStudentRepository.save(emailSendStudent);
//                            } catch (Exception e) {
//                                throw new RuntimeException(e);
//                            }
//                        }
//                );
//                return ResponseEntity.ok("Email sent!");
//
//            }
//
//        }
//    }
//
//    @PostMapping("reservation-successful")
//    public ResponseEntity<?> noticeReservation(@RequestParam List<Long> to,
//                                               @RequestParam String subject,
//                                               @RequestParam Long emailTemplateId,
//                                               @RequestParam Long userId) throws MessagingException, jakarta.mail.MessagingException {
//
//        emailSendService.sendStudents(to, subject, emailTemplateId, userId);
//        return ResponseEntity.ok("Email sent!");
//    }
//
//    @PostMapping("reservation-student")
//    public ResponseEntity<?> sendReservationStudent(
//            @RequestParam String subject,
//            @RequestParam Long emailTemplateId,
//            @RequestParam Long userId) throws MessagingException, jakarta.mail.MessagingException {
//        List<Student> reservateStudentList = new ArrayList<>();
//        reservateStudentList = studentRepository.findByStatus("Reserve");
//        reservateStudentList.forEach(s ->
//                System.out.println(s));
//        List<Long> studentIds = reservateStudentList.stream()
//                .map(Student::getStudentId)
//                .collect(Collectors.toList());
//
//        emailSendService.sendStudents(studentIds, subject, emailTemplateId, userId);
//        return ResponseEntity.ok("Email sent!");
//    }
//
//    @GetMapping("/findAll")
//    public ResponseEntity<?> findAllEmailWereSent(
//            @RequestParam(defaultValue = "0") int page,
//            @RequestParam(defaultValue = "10") int size) {
//        try {
//            Page<EmailSend> emailSends = emailSendService.findAllEmailWereSent(page,size);
//            return ResponseEntity.ok().body(emailSends);
//        } catch (FamsApiException e) {
//            return ResponseEntity.ok().body(
//                    Map.of("status", e.getStatus().value(), "message", e.getMessage())
//            );
//        }
//    }
}