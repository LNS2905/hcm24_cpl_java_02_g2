package com.fams.fams.controllers;

import com.fams.fams.models.entities.EmailTemplate;
import com.fams.fams.models.exception.FamsApiException;

import com.fams.fams.models.payload.dto.EmailTemplateDto;
import com.fams.fams.models.payload.requestModel.TemplateEmailAddNew;

import com.fams.fams.services.EmailTemplateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/email-template")
public class EmailTemplateController {
    private final EmailTemplateService emailTemplateService;

    public EmailTemplateController(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    @Operation(
            summary = "Get Email Templates",
            description = "Get List Email Templates in DB"
    )
    @ApiResponse(responseCode = "200", description = "Http Status 200 SUCCESS")
    @GetMapping("/templates")
    public ResponseEntity<?> getEmailTemplates(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        try {
            Page<EmailTemplateDto> emailTemplates = emailTemplateService.getAllEmailTemplate(page, size);
            return ResponseEntity.ok().body(emailTemplates);
        } catch (FamsApiException e) {
            return ResponseEntity.ok().body(
                    Map.of("status", e.getStatus().value(), "message", e.getMessage())
            );
        }
    }
//
//    @Operation(
//            summary = "Create New Email Template"
//    )
//    @ApiResponse(responseCode = "201", description = "Http Status 201 Created")
//    @SecurityRequirement(name = "Bear Authentication")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    @PostMapping("/email-template")
//    public ResponseEntity<?> createTemplate(@RequestBody TemplateEmailAddNew templateEmailAddNew) {
//        try {
//            emailTemplateService.createTemplate(templateEmailAddNew);
//            return ResponseEntity.ok().body(
//                    Map.of("status", 200, "message", "Add template email successfully!")
//            );
//        } catch (FamsApiException e) {
//            return ResponseEntity.ok().body(
//                    Map.of("status", e.getStatus().value(), "message", e.getMessage())
//            );
//        }
//    }
//    @Operation(
//            summary = "Create New Email Template"
//    )
//    @ApiResponse(responseCode = "201", description = "Http Status 201 Created")
//    @SecurityRequirement(name = "Bear Authentication")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    @PostMapping("/createV1")
//    public ResponseEntity<FamsApiException> createTemplateV1(@RequestBody TemplateEmailAddNew templateEmailAddNew){
//        return emailTemplateService.createTemplateV1(templateEmailAddNew);
//    }
//    @Operation(
//            summary = "Create New Email Template"
//    )
//    @ApiResponse(responseCode = "201", description = "Http Status 201 Created")
//    @SecurityRequirement(name = "Bear Authentication")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    @PutMapping("update-template/{id}")
//    public ResponseEntity<?> updateTemplate(@PathVariable("id") long templateId, @RequestBody EmailTemplateDto emailTemplateDto) {
//        try {
//            emailTemplateService.updateTemplate(templateId, emailTemplateDto);
//            return ResponseEntity.ok().body(
//                    Map.of("status", 200, "message", "Update template email successfully!")
//            );
//        } catch (FamsApiException e) {
//            return ResponseEntity.ok().body(
//                    Map.of("status", e.getStatus().value(), "message", e.getMessage())
//            );
//        }
//
//
//    }
//    @Operation(
//            summary = "Create New Email Template"
//    )
//    @ApiResponse(responseCode = "201", description = "Http Status 201 Created")
//    @SecurityRequirement(name = "Bear Authentication")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
//    @PutMapping("update-templateV1/{id}")
//    public ResponseEntity<FamsApiException> updateTemplateV1(@PathVariable("id") long templateId, @RequestBody EmailTemplateDto emailTemplateDto) {
//        return emailTemplateService.updateTemplateV1(templateId,emailTemplateDto);
//    }
//

}
