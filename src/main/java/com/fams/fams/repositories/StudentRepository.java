package com.fams.fams.repositories;

import com.fams.fams.models.entities.ReservedClass;
import com.fams.fams.models.entities.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByStudentCode(String studentCode);
    List<Student> findByEmail(String email);
    Page<Student> findAllByOrderByStudentId(Pageable pageable);

    @Query("select s from Student s where s.studentId = :id")
    Student findWithId(Long id);

    @Query("select s from Student  s where s.status = :status")
    List<Student> findByStatus(String status);
}

