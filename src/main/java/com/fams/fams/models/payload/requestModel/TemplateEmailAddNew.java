package com.fams.fams.models.payload.requestModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TemplateEmailAddNew {
    private LocalDate createDate;
    private String createBy;
    private String description;
    private String name;
    private String type;
}
