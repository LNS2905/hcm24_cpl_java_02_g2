package com.fams.fams.utils;

import com.fams.fams.models.entities.*;
import com.fams.fams.models.entities.Class;
import com.fams.fams.models.entities.Module;
import com.fams.fams.repositories.*;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class AddData {

    private final TrainingProgramRepository trainingProgramRepository;
    private final ModuleRepository moduleRepository;
    private final StudentRepository studentRepository;
    private final TrainingProgramModuleRepository trainingProgramModuleRepository;
    private final ClassRepository classRepository;
    private final StudentClassRepository studentClassRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AssignmentRepository assignmentRepository;
    private final EmailTemplateRepository emailTemplateRepository;

    @Autowired
    public AddData(TrainingProgramRepository trainingProgramRepository, ModuleRepository moduleRepository, StudentRepository studentRepository, TrainingProgramModuleRepository trainingProgramModuleRepository, ClassRepository classRepository, StudentClassRepository studentClassRepository, UserRepository userRepository, RoleRepository roleRepository, AssignmentRepository assignmentRepository, EmailTemplateRepository emailTemplateRepository) {
        this.trainingProgramRepository = trainingProgramRepository;
        this.moduleRepository = moduleRepository;
        this.studentRepository = studentRepository;
        this.trainingProgramModuleRepository = trainingProgramModuleRepository;
        this.classRepository = classRepository;
        this.studentClassRepository = studentClassRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.assignmentRepository = assignmentRepository;
        this.emailTemplateRepository = emailTemplateRepository;
    }

    @PostConstruct
    public void importData() {
//        Module module1 = new Module();
//        module1.setModuleName("Toán");
//        module1.setCreatedDate(LocalDate.now());
//        module1.setCreatedBy("admin");
//        module1.setUpdatedDate(LocalDate.now());
//        module1.setUpdatedBy("admin");
//        moduleRepository.save(module1);
//
//        Module module2 = new Module();
//        module2.setModuleName("Hóa");
//        module2.setCreatedDate(LocalDate.now());
//        module2.setCreatedBy("admin");
//        module2.setUpdatedDate(LocalDate.now());
//        module2.setUpdatedBy("admin");
//        moduleRepository.save(module2);
//
//        Module module3 = new Module();
//        module3.setModuleName("Sinh");
//        module3.setCreatedDate(LocalDate.now());
//        module3.setCreatedBy("admin");
//        module3.setUpdatedDate(LocalDate.now());
//        module3.setUpdatedBy("admin");
//        moduleRepository.save(module3);
//
//        Module module4 = new Module();
//        module4.setModuleName("Lịch sử");
//        module4.setCreatedDate(LocalDate.now());
//        module4.setCreatedBy("admin");
//        module4.setUpdatedDate(LocalDate.now());
//        module4.setUpdatedBy("admin");
//        moduleRepository.save(module4);
//
//        Module module5 = new Module();
//        module5.setModuleName("Địa lý");
//        module5.setCreatedDate(LocalDate.now());
//        module5.setCreatedBy("admin");
//        module5.setUpdatedDate(LocalDate.now());
//        module5.setUpdatedBy("admin");
//        moduleRepository.save(module5);
//
//        Module module6 = new Module();
//        module6.setModuleName("FEE");
//        module6.setCreatedDate(LocalDate.now());
//        module6.setCreatedBy("admin");
//        module6.setUpdatedDate(LocalDate.now());
//        module6.setUpdatedBy("admin");
//        moduleRepository.save(module6);
//
//        Module module7 = new Module();
//        module7.setModuleName("MOCK");
//        module7.setCreatedDate(LocalDate.now());
//        module7.setCreatedBy("admin");
//        module7.setUpdatedDate(LocalDate.now());
//        module7.setUpdatedBy("admin");
//        moduleRepository.save(module7);
//
//        TrainingProgram trainingProgram1 = new TrainingProgram();
//        trainingProgram1.setName("Chương trình A");
//        trainingProgram1.setStatus("Active");
//        trainingProgram1.setCode("A001");
//        trainingProgram1.setCreateDate(LocalDate.now());
//        trainingProgram1.setDuration(1.0f); // Giả sử mỗi module học trong 1 tháng
//        trainingProgram1.setCreatedBy("admin");
//        trainingProgram1.setUpdateDate(LocalDate.now());
//        trainingProgram1.setUpdatedBy("admin");
//        trainingProgramRepository.save(trainingProgram1);
//
//        TrainingProgram trainingProgram2 = new TrainingProgram();
//        trainingProgram2.setName("Chương trình B");
//        trainingProgram2.setStatus("Active");
//        trainingProgram2.setCode("B001");
//        trainingProgram2.setCreateDate(LocalDate.now());
//        trainingProgram2.setDuration(1.0f); // Giả sử mỗi module học trong 1 tháng
//        trainingProgram2.setCreatedBy("admin");
//        trainingProgram2.setUpdateDate(LocalDate.now());
//        trainingProgram2.setUpdatedBy("admin");
//        trainingProgramRepository.save(trainingProgram2);
//
//        TrainingProgram trainingProgram3 = new TrainingProgram();
//        trainingProgram3.setName("Chương trình A");
//        trainingProgram3.setStatus("Active");
//        trainingProgram3.setCode("BE_JAVA_SPRING");
//        trainingProgram3.setCreateDate(LocalDate.now());
//        trainingProgram3.setDuration(1.0f); // Giả sử mỗi module học trong 1 tháng
//        trainingProgram3.setCreatedBy("admin");
//        trainingProgram3.setUpdateDate(LocalDate.now());
//        trainingProgram3.setUpdatedBy("admin");
//        trainingProgramRepository.save(trainingProgram3);
//
//        Student student1 = new Student();
//        student1.setStudentCode("SV001");
//        student1.setFullName("Nguyễn Văn A");
//        student1.setDOB(LocalDate.of(1995, 5, 10));
//        student1.setGender("Nam");
//        student1.setPhone("0123456789");
//        student1.setEmail("student1@example.com");
//        student1.setSchool("Trường A");
//        student1.setMajor("Ngành A");
//        student1.setGraduatedDate(LocalDate.of(2020, 6, 15));
//        student1.setGPA(3.8f);
//        student1.setAddress("Địa chỉ A");
//        student1.setFAAccount("fa_account_1");
//        student1.setType("Loại A");
//        student1.setStatus("Hoạt động");
//        student1.setRECer("RECer A");
//        student1.setJoinedDate(LocalDate.now());
//        student1.setArea("Khu vực A");
//        studentRepository.save(student1);
//
//        // Sinh viên 2
//        Student student2 = new Student();
//        student2.setStudentCode("SV002");
//        student2.setFullName("Trần Thị B");
//        student2.setDOB(LocalDate.of(1996, 7, 20));
//        student2.setGender("Nữ");
//        student2.setPhone("0987654321");
//        student2.setEmail("student2@example.com");
//        student2.setSchool("Trường B");
//        student2.setMajor("Ngành B");
//        student2.setGraduatedDate(LocalDate.of(2019, 5, 20));
//        student2.setGPA(3.6f);
//        student2.setAddress("Địa chỉ B");
//        student2.setFAAccount("fa_account_2");
//        student2.setType("Loại B");
//        student2.setStatus("Hoạt động");
//        student2.setRECer("RECer B");
//        student2.setJoinedDate(LocalDate.now());
//        student2.setArea("Khu vực B");
//        studentRepository.save(student2);
//
//        Class class1 = new Class();
//        class1.setClassCode("ABC123");
//        class1.setClassName("Mathematics");
//        class1.setStartDate(LocalDate.now().plusMonths(10));
//        class1.setEndDate(LocalDate.now().plusMonths(13));
//        class1.setCreatedDate(LocalDate.now());
//        class1.setCreatedBy("John Doe");
//        class1.setUpdatedDate(LocalDate.now());
//        class1.setUpdatedBy("John Doe");
//        class1.setDuration(3.5f);
//        class1.setLocation("Room A");
//        class1.setStatus("Active");
//        class1.setTrainingPrograms(trainingProgram1);
//        classRepository.save(class1);
//
//        Class class2 = new Class();
//        class2.setClassCode("DEF456");
//        class2.setClassName("English");
//        class2.setStartDate(LocalDate.now().minusMonths(1));
//        class2.setEndDate(LocalDate.now().plusMonths(1));
//        class2.setCreatedDate(LocalDate.now());
//        class2.setCreatedBy("Jane Smith");
//        class2.setUpdatedDate(LocalDate.now());
//        class2.setUpdatedBy("Jane Smith");
//        class2.setDuration(4.0f);
//        class2.setLocation("Room B");
//        class2.setStatus("Active");
//        class2.setTrainingPrograms(trainingProgram1);
//        classRepository.save(class2);
//
//        Class class3 = new Class();
//        class3.setClassCode("DEF4567");
//        class3.setClassName("BE");
//        class3.setStartDate(LocalDate.now().minusMonths(1));
//        class3.setEndDate(LocalDate.now().plusMonths(1));
//        class3.setCreatedDate(LocalDate.now());
//        class3.setCreatedBy("Jane Smith");
//        class3.setUpdatedDate(LocalDate.now());
//        class3.setUpdatedBy("Jane Smith");
//        class3.setDuration(4.0f);
//        class3.setLocation("Room B");
//        class3.setStatus("Active");
//        class3.setTrainingPrograms(trainingProgram3);
//        classRepository.save(class3);
//
//        TrainingProgramModule trainingProgramModule1 = new TrainingProgramModule();
//        trainingProgramModule1.setModules(module1);
//        trainingProgramModule1.setTrainingPrograms(trainingProgram1);
//        trainingProgramModuleRepository.save(trainingProgramModule1);
//
//        TrainingProgramModule trainingProgramModule2 = new TrainingProgramModule();
//        trainingProgramModule2.setModules(module2);
//        trainingProgramModule2.setTrainingPrograms(trainingProgram1);
//        trainingProgramModuleRepository.save(trainingProgramModule2);
//
//        TrainingProgramModule trainingProgramModule3 = new TrainingProgramModule();
//        trainingProgramModule3.setModules(module3);
//        trainingProgramModule3.setTrainingPrograms(trainingProgram1);
//        trainingProgramModuleRepository.save(trainingProgramModule3);
//
//        TrainingProgramModule trainingProgramModule4 = new TrainingProgramModule();
//        trainingProgramModule4.setModules(module4);
//        trainingProgramModule4.setTrainingPrograms(trainingProgram2);
//        trainingProgramModuleRepository.save(trainingProgramModule4);
//
//        TrainingProgramModule trainingProgramModule5 = new TrainingProgramModule();
//        trainingProgramModule5.setModules(module5);
//        trainingProgramModule5.setTrainingPrograms(trainingProgram2);
//        trainingProgramModuleRepository.save(trainingProgramModule5);
//
//        TrainingProgramModule trainingProgramModule6 = new TrainingProgramModule();
//        trainingProgramModule6.setModules(module6);
//        trainingProgramModule6.setTrainingPrograms(trainingProgram3);
//        trainingProgramModuleRepository.save(trainingProgramModule6);
//
//        TrainingProgramModule trainingProgramModule7 = new TrainingProgramModule();
//        trainingProgramModule7.setModules(module7);
//        trainingProgramModule7.setTrainingPrograms(trainingProgram3);
//        trainingProgramModuleRepository.save(trainingProgramModule7);
//
//        StudentClass entity1 = new StudentClass();
//        entity1.setStudents(student1);
//        entity1.setClasses(class1);
//        entity1.setAttendingStatus("InClass");
//        entity1.setResult(true);
//        entity1.setFinalScore(8.5f);
//        entity1.setGPALevel("A");
//        entity1.setCertificationStatus("Certified");
//        entity1.setCertificationDate(LocalDate.now());
//        entity1.setMethod("Online");
//        studentClassRepository.save(entity1);
//
//        StudentClass entity2 = new StudentClass();
//        entity2.setStudents(student1);
//        entity2.setClasses(class2);
//        entity2.setAttendingStatus("InClass");
//        entity2.setResult(false);
//        entity2.setFinalScore(6.0f);
//        entity2.setGPALevel("C");
//        entity2.setCertificationStatus("Uncertified");
//        entity2.setCertificationDate(LocalDate.now());
//        entity2.setMethod("In-person");
//        studentClassRepository.save(entity2);
//
//        StudentClass entity3 = new StudentClass();
//        entity3.setStudents(student2);
//        entity3.setClasses(class1);
//        entity3.setAttendingStatus("InClass");
//        entity3.setResult(true);
//        entity3.setFinalScore(8.5f);
//        entity3.setGPALevel("A");
//        entity3.setCertificationStatus("Certified");
//        entity3.setCertificationDate(LocalDate.now());
//        entity3.setMethod("Online");
//        studentClassRepository.save(entity3);
//
//        Role role1 = new Role();
//        role1.setRoleName("ROLE_ADMIN");
//        roleRepository.save(role1);
//
//        Role role2 = new Role();
//        role2.setRoleName("ROLE_TRAINER");
//        roleRepository.save(role2);
//
//        User user1 = new User();
//        user1.setFullName("John Doe");
//        user1.setEmail("john.doe@example.com");
//        user1.setDOB(LocalDate.of(1990, 5, 15));
//        user1.setAddress("123 Main St, City, Country");
//        user1.setGender("Male");
//        user1.setPhone("1234567890");
//        user1.setUserName("admin");
//        user1.setPassword("$2a$10$IXDGgm..gMBfSu6LnAAr0e.l1IGMqdEuhxfQxaJa3zP2JaB9Al/fO");
//        user1.setRole(role1);
//        userRepository.save(user1);
//
//        User user2 = new User();
//        user2.setFullName("Jane Smith");
//        user2.setEmail("jane.smith@example.com");
//        user2.setDOB(LocalDate.of(1985, 10, 25));
//        user2.setAddress("456 Elm St, Town, Country");
//        user2.setGender("Female");
//        user2.setPhone("9876543210");
//        user2.setUserName("trainer");
//        user2.setPassword("$2a$10$RUaMxQo3nuA.Tc0YybonVeU.xHt3OYwNSopRoTkhCZmik/K5NOTne");
//        user2.setRole(role2);
//        userRepository.save(user2);
//
//        Assignment assignment1 = new Assignment();
//        assignment1.setAssignmentName("Quiz 3");
//        assignment1.setAssignmentType("Quiz");
//        assignment1.setCreatedBy("DoNVC");
//        assignment1.setCreatedDate(LocalDate.of(2024, 1, 20)); // Note: Month is 0-based
//        assignment1.setDescription("Random description");
//        assignment1.setDueDate(LocalDate.of(2024, 1, 25)); // Note: Month is 0-based
//        assignment1.setUpdatedBy("DoNVC");
//        assignment1.setUpdatedDate(LocalDate.of(2024, 1, 20)); // Note: Month is 0-based
//        assignment1.setModules(module6);
//        assignmentRepository.save(assignment1);
//
//        Assignment assignment2 = new Assignment();
//        assignment2.setAssignmentName("Quiz 4");
//        assignment2.setAssignmentType("Quiz");
//        assignment2.setCreatedBy("DoNVC");
//        assignment2.setCreatedDate(LocalDate.of(2024, 1, 21)); // Note: Month is 0-based
//        assignment2.setDescription("Random description");
//        assignment2.setDueDate(LocalDate.of(2024, 1, 26)); // Note: Month is 0-based
//        assignment2.setUpdatedBy("DoNVC");
//        assignment2.setUpdatedDate(LocalDate.of(2024, 1, 21)); // Note: Month is 0-based
//        assignment2.setModules(module6);
//        assignmentRepository.save(assignment2);
//
//        Assignment assignment3 = new Assignment();
//        assignment3.setAssignmentName("Quiz 5");
//        assignment3.setAssignmentType("Quiz");
//        assignment3.setCreatedBy("DoNVC");
//        assignment3.setCreatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment3.setDescription("Random description");
//        assignment3.setDueDate(LocalDate.of(2024, 1, 27)); // Note: Month is 0-based
//        assignment3.setUpdatedBy("DoNVC");
//        assignment3.setUpdatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment3.setModules(module6);
//        assignmentRepository.save(assignment3);
//
//        Assignment assignment4 = new Assignment();
//        assignment4.setAssignmentName("Quiz 6");
//        assignment4.setAssignmentType("Quiz");
//        assignment4.setCreatedBy("DoNVC");
//        assignment4.setCreatedDate(LocalDate.of(2024, 1, 23)); // Note: Month is 0-based
//        assignment4.setDescription("Random description");
//        assignment4.setDueDate(LocalDate.of(2024, 1, 28)); // Note: Month is 0-based
//        assignment4.setUpdatedBy("DoNVC");
//        assignment4.setUpdatedDate(LocalDate.of(2024, 1, 23)); // Note: Month is 0-based
//        assignment4.setModules(module6);
//        assignmentRepository.save(assignment4);
//
//        Assignment assignment5 = new Assignment();
//        assignment5.setAssignmentName("Average");
//        assignment5.setAssignmentType("Quiz");
//        assignment5.setCreatedBy("DoNVC");
//        assignment5.setCreatedDate(LocalDate.of(2024, 1, 24)); // Note: Month is 0-based
//        assignment5.setDescription("Random description");
//        assignment5.setDueDate(LocalDate.of(2024, 1, 29)); // Note: Month is 0-based
//        assignment5.setUpdatedBy("DoNVC");
//        assignment5.setUpdatedDate(LocalDate.of(2024, 1, 24)); // Note: Month is 0-based
//        assignment5.setModules(module6);
//        assignmentRepository.save(assignment5);
//
//        Assignment assignment6 = new Assignment();
//        assignment6.setAssignmentName("Practice Exam 1");
//        assignment6.setAssignmentType("ASM");
//        assignment6.setCreatedBy("DoNVC");
//        assignment6.setCreatedDate(LocalDate.of(2024, 1, 20)); // Note: Month is 0-based
//        assignment6.setDescription("Random description");
//        assignment6.setDueDate(LocalDate.of(2024, 1, 25)); // Note: Month is 0-based
//        assignment6.setUpdatedBy("DoNVC");
//        assignment6.setUpdatedDate(LocalDate.of(2024, 1, 20)); // Note: Month is 0-based
//        assignment6.setModules(module6);
//        assignmentRepository.save(assignment6);
//
//        Assignment assignment7 = new Assignment();
//        assignment7.setAssignmentName("Practice Exam 2");
//        assignment7.setAssignmentType("ASM");
//        assignment7.setCreatedBy("DoNVC");
//        assignment7.setCreatedDate(LocalDate.of(2024, 1, 21)); // Note: Month is 0-based
//        assignment7.setDescription("Random description");
//        assignment7.setDueDate(LocalDate.of(2024, 1, 26)); // Note: Month is 0-based
//        assignment7.setUpdatedBy("DoNVC");
//        assignment7.setUpdatedDate(LocalDate.of(2024, 1, 21)); // Note: Month is 0-based
//        assignment7.setModules(module6);
//        assignmentRepository.save(assignment7);
//
//        Assignment assignment8 = new Assignment();
//        assignment8.setAssignmentName("Practice Exam 3");
//        assignment8.setAssignmentType("ASM");
//        assignment8.setCreatedBy("DoNVC");
//        assignment8.setCreatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment8.setDescription("Random description");
//        assignment8.setDueDate(LocalDate.of(2024, 1, 27)); // Note: Month is 0-based
//        assignment8.setUpdatedBy("DoNVC");
//        assignment8.setUpdatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment8.setModules(module6);
//        assignmentRepository.save(assignment8);
//
//        Assignment assignment9 = new Assignment();
//        assignment9.setAssignmentName("Average");
//        assignment9.setAssignmentType("ASM");
//        assignment9.setCreatedBy("DoNVC");
//        assignment9.setCreatedDate(LocalDate.of(2024, 1, 23)); // Note: Month is 0-based
//        assignment9.setDescription("Random description");
//        assignment9.setDueDate(LocalDate.of(2024, 1, 27)); // Note: Month is 0-based
//        assignment9.setUpdatedBy("DoNVC");
//        assignment9.setUpdatedDate(LocalDate.of(2024, 1, 23)); // Note: Month is 0-based
//        assignment9.setModules(module6);
//        assignmentRepository.save(assignment9);
//
//        Assignment assignment10 = new Assignment();
//        assignment10.setAssignmentName("Quiz Final");
//        assignment10.setAssignmentType("Quiz Final");
//        assignment10.setCreatedBy("DoNVC");
//        assignment10.setCreatedDate(LocalDate.of(2024, 1, 21)); // Note: Month is 0-based
//        assignment10.setDescription("Random description");
//        assignment10.setDueDate(LocalDate.of(2024, 1, 26)); // Note: Month is 0-based
//        assignment10.setUpdatedBy("DoNVC");
//        assignment10.setUpdatedDate(LocalDate.of(2024, 1, 21)); // Note: Month is 0-based
//        assignment10.setModules(module6);
//        assignmentRepository.save(assignment10);
//
//        Assignment assignment11 = new Assignment();
//        assignment11.setAssignmentName("Audit");
//        assignment11.setAssignmentType("Audit");
//        assignment11.setCreatedBy("DoNVC");
//        assignment11.setCreatedDate(LocalDate.of(2024, 1, 20)); // Note: Month is 0-based
//        assignment11.setDescription("Random description");
//        assignment11.setDueDate(LocalDate.of(2024, 1, 25)); // Note: Month is 0-based
//        assignment11.setUpdatedBy("DoNVC");
//        assignment11.setUpdatedDate(LocalDate.of(2024, 1, 20)); // Note: Month is 0-based
//        assignment11.setModules(module6);
//        assignmentRepository.save(assignment11);
//
//        Assignment assignment12 = new Assignment();
//        assignment12.setAssignmentName("Practice Final");
//        assignment12.setAssignmentType("Practice Final");
//        assignment12.setCreatedBy("DoNVC");
//        assignment12.setCreatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment12.setDescription("Random description");
//        assignment12.setDueDate(LocalDate.of(2024, 1, 27)); // Note: Month is 0-based
//        assignment12.setUpdatedBy("DoNVC");
//        assignment12.setUpdatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment12.setModules(module6);
//        assignmentRepository.save(assignment12);
//
//        Assignment assignment13 = new Assignment();
//        assignment13.setAssignmentName("MOCK");
//        assignment13.setAssignmentType("MOCK");
//        assignment13.setCreatedBy("DoNVC");
//        assignment13.setCreatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment13.setDescription("Random description");
//        assignment13.setDueDate(LocalDate.of(2024, 1, 27)); // Note: Month is 0-based
//        assignment13.setUpdatedBy("DoNVC");
//        assignment13.setUpdatedDate(LocalDate.of(2024, 1, 22)); // Note: Month is 0-based
//        assignment13.setModules(module7);
//        assignmentRepository.save(assignment13);
//
//        for (int i = 1; i <= 5; i++) {
//            EmailTemplate emailTemplate1 = new EmailTemplate();
//            emailTemplate1.setType("Type" + i);
//            emailTemplate1.setName("Name" + i);
//            emailTemplate1.setDescription("Description" + 1);
//            emailTemplate1.setCreatedDate(LocalDate.now().plusDays(i));
//            emailTemplate1.setCreatedBy("Admin");
//            emailTemplate1.setUpdatedDate(LocalDate.now().plusDays(1));
//            emailTemplate1.setUpdatedBy("Admin");
//            emailTemplate1.setStatus(true);
//            emailTemplateRepository.save(emailTemplate1);
//        }

    }
}
