package com.fams.fams.utils;

public class Template {
    public String reservationStatus(String receiver, String trainer, String phone) {
        return "<p>Hello " + receiver + ",</p>"
                + "<p>Good day to you! I am " + trainer+ ", the Admin of Fresher Academy"
                + "</p>"
                + "<p>We are delighted that you have decided to retain your enrollment at Fresher Academy to maintain your knowledge and skills. Currently, I would like to remind you that your retention period will end in about a month.</p>"
                + "<br>"
                + "<p>To help you easily return and continue your learning journey, we suggest that you contact us before your retention period expires. This way, we can assist you in the class placement process and ensure that you will have the best learning experience at Fresher Academy.</p>"
                +"<p>Please reach out to us via the phone number:" + phone +". We will be happy to support you with any inquiries or requests you may have.</p>"
                +"<p>Thank you sincerely for your interest and commitment to the program at Fresher Academy. We look forward to welcoming you back and sharing new knowledge with you.</p>"
                +"<h3>Best regards,</h3>"
                +"<p>" + trainer +"</p>"
                +"<p>Fresher Academy Admin</p>";
    }

    public String informReservationSuccessful(String receiver, String trainer) {
        return  "<p>Dear " + receiver + ",</p>"
                +"<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>"
                +"<h3>Best regards,</h3>"
                +"<p>" + trainer +"</p>"
                +"<p>Fresher Academy Admin</p>";
    }

}
