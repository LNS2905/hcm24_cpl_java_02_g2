package com.fams.fams.services;

import com.fams.fams.models.entities.Class;
import org.springframework.data.domain.Page;

public interface ClassService {
    Page<Class> getClassesByStudentId(Long studentId, int page, int size);
    Page<Class> getAllClass(int page, int size);
}
