package com.fams.fams.services.impl;

import com.fams.fams.models.entities.*;
import com.fams.fams.models.exception.FamsApiException;
import com.fams.fams.repositories.*;
import com.fams.fams.services.EmailSendService;
import com.fams.fams.services.EmailTemplateService;
import com.fams.fams.services.StudentService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Transactional
public class EmailSendServiceImpl implements EmailSendService {
    @Autowired
    private JavaMailSender javaMailSender;
    private StudentService studentService;
    private EmailTemplateService emailTemplateService;
    private EmailSendRepository emailSendRepository;
    private StudentRepository studentRepository;
    private EmailSendStudentRepository emailSendStudentRepository;
    private EmailSendUserRepository emailSendUserRepository;
    private UserRepository userRepository;

    @Autowired
    public EmailSendServiceImpl(StudentService studentService,EmailTemplateService emailTemplateService, EmailSendRepository emailSendRepository, StudentRepository studentRepository, EmailSendStudentRepository emailSendStudentRepository, EmailSendUserRepository emailSendUserRepository, UserRepository userRepository) {
        this.studentService = studentService;
        this.emailTemplateService = emailTemplateService;
        this.emailSendRepository = emailSendRepository;
        this.studentRepository = studentRepository;
        this.emailSendStudentRepository = emailSendStudentRepository;
        this.emailSendUserRepository = emailSendUserRepository;
        this.userRepository = userRepository;
    }

//    @Override
//    public JavaMailSender getJavaMailSender() {
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost("smtp.gmail.com");
//        mailSender.setPort(587); // Default SMTP port for TLS
//        mailSender.setUsername("fams.college.001@gmail.com");
//        mailSender.setPassword("nkba tocj kfus ksla");
//
//        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", "smtp");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//
//        return mailSender;
//    }
//    @Override
//    public void sendStudents(List<Long> studnetIds, String subject, Long emailTemplateId, Long userId) throws MessagingException, MessagingException {
//        try {
//            MimeMessage msg = javaMailSender.createMimeMessage();
//            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//            List<Student> studentsLists = new ArrayList<>();
//            Set<Long> uniqueUserIds = new HashSet<>(studnetIds);
//            if (userRepository.findWithId(userId) == null)
//                throw new FamsApiException(HttpStatus.NOT_FOUND, "Trainer not found");
//            if (uniqueUserIds.size() != studnetIds.size()) {
//                throw new FamsApiException(HttpStatus.CONFLICT,"Students are duplicated!");
//            }
//            List<Long> invalidList = new ArrayList<>();
//            studnetIds.forEach(s -> {
//                var temp = studentRepository.findWithId(s);
//                if(temp != null) studentsLists.add(temp);
//                else invalidList.add(s);
//            });
//
//            helper.setFrom("fams.college.001@gmail.com");
//            helper.setTo(
//                    studentsLists.stream()
//                            .map(Student::getEmail)
//                            .toArray(String[]::new)
//            );
//            helper.setSubject(subject);
//            helper.setText(emailTemplateService.getTemplateDetails(emailTemplateId).getDescription(), true);
//            if (invalidList.isEmpty()){
//                javaMailSender.send(msg);
//
//
//                EmailSend emailSend = new EmailSend();
//                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//                LocalDate now = LocalDate.now();
//                emailSend.setSendDate(now);
//                emailSend.setEmailTemplates(emailTemplateService.getTemplateDetails(emailTemplateId));
//                emailSend.setContent(subject);
//                emailSend.setReceiverType("emailSendStudents");
//                emailSend.setUsers(userRepository.findWithId(userId));
//                var emailSendResult = emailSendRepository.save(emailSend);
//
//                List<EmailSendStudent> emailSendStudentsList = new ArrayList<>();
//                studentsLists.forEach(o -> {
//                    var temp = new EmailSendStudent();
//                    temp.setStudents(o);
//                    temp.setEmailSends(emailSendResult);
//                    emailSendStudentsList.add(temp);
//                });
//                emailSendStudentRepository.saveAll(emailSendStudentsList);
//            }
//            else
//                throw new FamsApiException(HttpStatus.NOT_FOUND, "Students not found!" + invalidList);
//        }catch (Exception e){
//            throw new RuntimeException(e);
//        }
//    }
//
//    @Override
//    public void sendTrainers(List<Long> userIds, String subject, Long emailTemplateId,Long userId) throws MessagingException, MessagingException {
//        try {
//            MimeMessage msg = javaMailSender.createMimeMessage();
//            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//            if (userRepository.findWithId(userId) == null)
//                throw new FamsApiException(HttpStatus.NOT_FOUND, "Traner not found");
//            List<User> userList = new ArrayList<>();
//            Set<Long> uniqueUserIds = new HashSet<>(userIds);
//            if (uniqueUserIds.size() != userIds.size()) {
//                throw new FamsApiException(HttpStatus.CONFLICT,"Trainers are duplicated!");
//            }
//            List<Long> invalidList = new ArrayList<>();
//            userIds.forEach(u -> {
//                var temp = userRepository.findWithId(u);
//                if(temp != null) userList.add(temp);
//                else invalidList.add(u);
//            });
//            helper.setFrom("fams.college.001@gmail.com");
//            helper.setTo(
//                    userList.stream()
//                            .map(User::getEmail)
//                            .toArray(String[]::new)
//            );
//            helper.setSubject(subject);
//            helper.setText(emailTemplateService.getTemplateDetails(emailTemplateId).getDescription(), true);
//            if (invalidList.isEmpty()){
//                javaMailSender.send(msg);
//
//
//                EmailSend emailSend = new EmailSend();
//                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//                LocalDate now = LocalDate.now();
//                emailSend.setSendDate(now);
//                emailSend.setEmailTemplates(emailTemplateService.getTemplateDetails(emailTemplateId));
//                emailSend.setContent(subject);
//                emailSend.setReceiverType("emailSendTrainers");
//                emailSend.setUsers(userRepository.findWithId(userId));
//                var emailSendResult = emailSendRepository.save(emailSend);
//
//                List<EmailSendUser> emailSendUsersList = new ArrayList<>();
//                userList.forEach(o -> {
//                    var temp = new EmailSendUser();
//                    temp.setUsers(o);
//                    temp.setEmailSends(emailSendResult);
//                    emailSendUsersList.add(temp);
//                });
//                emailSendUserRepository.saveAll(emailSendUsersList);
//            }
//            else
//                throw new FamsApiException(HttpStatus.NOT_FOUND, "Users not found!" + invalidList);
//
//
//        }catch (Exception e){
//            throw new RuntimeException(e);
//        }
//    }
//
//
//    @Override
//    public void informReservation(String email, String subject, String message) throws MessagingException, MessagingException {
//        MimeMessage msg = javaMailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//        helper.setFrom("fams.college.001@gmail.com");
//        helper.setTo(email);
//        helper.setSubject(subject);
//        helper.setText(message, true);
//        javaMailSender.send(msg);
//    }
//
//    @Override
//    public Page<EmailSend> findAllEmailWereSent(int page, int size) {
//        PageRequest pageable = PageRequest.of(page, size);
//        Page<EmailSend> emailSends = emailSendRepository.findAll(pageable);
//        if (emailSends.isEmpty()) {
//            throw new FamsApiException(HttpStatus.NOT_FOUND, "Email were sent not found!");
//        } else {
//            return emailSends;
//        }
//    }
}



