package com.fams.fams.services.impl;

import com.fams.fams.models.entities.EmailTemplate;
import com.fams.fams.models.exception.FamsApiException;
import com.fams.fams.models.payload.dto.EmailTemplateDto;
import com.fams.fams.models.payload.requestModel.TemplateEmailAddNew;
import com.fams.fams.repositories.EmailTemplateRepository;
import com.fams.fams.services.EmailTemplateService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmailTemplateImpl implements EmailTemplateService {
    private final EmailTemplateRepository emailTemplateRepository;
    public final ModelMapper mapper;

    public EmailTemplateImpl(EmailTemplateRepository emailTemplateRepository, ModelMapper mapper) {
        this.emailTemplateRepository = emailTemplateRepository;
        this.mapper = mapper;
    }

//    @Override
//    public void createTemplate(TemplateEmailAddNew templateEmailAddNew) {
//        try {
//            EmailTemplate emailTemplate = mapPayloadToEmailtemplateEntity(templateEmailAddNew);
//            emailTemplate.setUpdatedBy("not yet");
//            emailTemplateRepository.save(emailTemplate);
//        } catch (Exception e) {
//            throw  new FamsApiException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to save template!");
//        }
//    }
//    @Override
//    public void updateTemplate(long templateId, EmailTemplateDto emailTemplateDto) {
//        Optional<EmailTemplate> optionalTemplate = emailTemplateRepository.findById(templateId);
//        if (optionalTemplate.isEmpty()) {
//            throw new FamsApiException(HttpStatus.NOT_FOUND, "NOT FOUND TEMPLATE ID: " + templateId);
//        }
//
//        EmailTemplate template = optionalTemplate.get();
//        try {
////            if (emailTemplateDto.getUpdateBy() == null || emailTemplateDto.getUpdateBy().isEmpty() ||
////                    emailTemplateDto.getUpdateDate() == null) {
////                throw new FamsApiException(HttpStatus.BAD_REQUEST, "UpdateBy and UpdateDate cannot be null or empty.");
////            }
//            if (emailTemplateDto.getUpdateBy() == null || emailTemplateDto.getUpdateBy().isEmpty()) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Update by' must not be null or empty.");
//            } else if (emailTemplateDto.getUpdateDate() == null ) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Update date' must not be null or empty.");
//            } else if (emailTemplateDto.getDescription() == null || emailTemplateDto.getDescription().isEmpty() ) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Description' must not be null or empty.");
//            } else if (emailTemplateDto.getName() == null || emailTemplateDto.getName().isEmpty() ) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Name template'  must not be null or empty.");
//            } else if (emailTemplateDto.getType() == null || emailTemplateDto.getType().isEmpty() ) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Type template' fields must not be null or empty.");
//            } else if (emailTemplateDto.getCreateDate() == null) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Date create' must not be null or empty.");
//            } else if (emailTemplateDto.getCreateBy() == null || emailTemplateDto.getCreateBy().isEmpty()) {
//                throw new FamsApiException(HttpStatus.BAD_REQUEST, "'Create by' must not be null or empty.");
//            }
//            template.setUpdatedBy(emailTemplateDto.getUpdateBy());
//            template.setCreatedBy(emailTemplateDto.getUpdateBy());
//            template.setDescription(emailTemplateDto.getDescription());
//            template.setUpdatedDate(emailTemplateDto.getUpdateDate());
//            template.setCreatedDate(emailTemplateDto.getCreateDate());
//            template.setName(emailTemplateDto.getName());
//            template.setType(emailTemplateDto.getType());
//            emailTemplateRepository.save(template);
//        }
//        catch (FamsApiException e) {
//            throw e;
//        }catch (Exception e) {
//            throw new FamsApiException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to save template!");
//        }
//    }
//
//    //return response
//    @Override
//    public ResponseEntity<FamsApiException> updateTemplateV1(long templateId, EmailTemplateDto emailTemplateDto) {
//        try {
//            Optional<EmailTemplate> optionalTemplate = emailTemplateRepository.findById(templateId);
//            if (optionalTemplate.isEmpty()) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.NOT_FOUND, "NOT FOUND TEMPLATE"));
//            }
//
//            EmailTemplate template = optionalTemplate.get();
//            if (emailTemplateDto.getUpdateBy() == null || emailTemplateDto.getUpdateBy().isEmpty()) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Update by' must not be null or empty."));
//            } else if (emailTemplateDto.getUpdateDate() == null ) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Update date' must not be null or empty."));
//
//            } else if (emailTemplateDto.getDescription() == null || emailTemplateDto.getDescription().isEmpty() ) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Description' must not be null or empty."));
//
//            } else if (emailTemplateDto.getName() == null || emailTemplateDto.getName().isEmpty() ) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Name template'  must not be null or empty."));
//
//            } else if (emailTemplateDto.getType() == null || emailTemplateDto.getType().isEmpty() ) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Type template' fields must not be null or empty."));
//
//            } else if (emailTemplateDto.getCreateDate() == null) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Date create' must not be null or empty."));
//            }else if (emailTemplateDto.getCreateBy() == null || emailTemplateDto.getCreateBy().isEmpty()) {
//                return ResponseEntity.ok().body(new FamsApiException(HttpStatus.BAD_REQUEST, "'Create by' must not be null or empty."));
//            }
//            template.setUpdatedBy(emailTemplateDto.getUpdateBy());
//            template.setCreatedBy(emailTemplateDto.getUpdateBy());
//            template.setDescription(emailTemplateDto.getDescription());//
//            template.setUpdatedDate(emailTemplateDto.getUpdateDate());//
//            template.setCreatedDate(emailTemplateDto.getCreateDate());//
//            template.setName(emailTemplateDto.getName());//
//            template.setType(emailTemplateDto.getType());//
//            emailTemplateRepository.save(template);
//            return ResponseEntity.ok().body(new FamsApiException(HttpStatus.OK, "Template updated successfully."));
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().body(new FamsApiException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to update template: " + e.getMessage()));
//        }
//    }
//
//    //return response
//    @Override
//    public ResponseEntity<FamsApiException> createTemplateV1(TemplateEmailAddNew templateEmailAddNew) {
//        try {
//            EmailTemplate emailTemplate = mapPayloadToEmailtemplateEntity(templateEmailAddNew);
//            emailTemplate.setUpdatedBy("not yet");
//            emailTemplateRepository.save(emailTemplate);
//            return ResponseEntity.ok().body(new FamsApiException(HttpStatus.OK, "Template crate successfully."));
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().body(new FamsApiException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to create template: " + e.getMessage()));
//
//        }
//    }
//
//    @Override
//    public EmailTemplate getTemplateDetails(Long id) {
//        EmailTemplate emailTemplate = emailTemplateRepository.findById(id).orElseThrow(()-> new FamsApiException(HttpStatus.NOT_FOUND, "Template not found!!!"));
//        return emailTemplate;
//    }
//
//    public EmailTemplate mapToEmailtemplateDto(TemplateEmailAddNew templateEmailAddNew){
//        EmailTemplate emailTemplate = new EmailTemplate();
//
//        emailTemplate.setCreatedDate(templateEmailAddNew.getCreateDate());
//        emailTemplate.setCreatedBy(templateEmailAddNew.getCreateBy());
//        emailTemplate.setDescription(templateEmailAddNew.getDescription());
//        emailTemplate.setName(templateEmailAddNew.getName());
//        emailTemplate.setType(templateEmailAddNew.getType());
////        emailTemplate.setUpdatedBy(templateEmailPayload.getUpdateBy());
////        emailTemplate.setUpdatedDate(templateEmailPayload.getUpdateDate());
//        return  emailTemplate;
//    }

    @Override
    public Page<EmailTemplateDto> getAllEmailTemplate(int page, int size) {
        PageRequest pageable = PageRequest.of(page, size);
        Page<EmailTemplate> emailTemplates = emailTemplateRepository.findAll(pageable);
        if (emailTemplates.isEmpty()) {
            throw new FamsApiException(HttpStatus.NOT_FOUND, "Email Templates not found!");
        } else {
            return emailTemplates.map(this::mapToEmailTemplateDto);
        }
    }

    public EmailTemplateDto mapToEmailTemplateDto(EmailTemplate emailTemplate){
        return mapper.map(emailTemplate, EmailTemplateDto.class);
    }
}
