package com.fams.fams.services.impl;

import com.fams.fams.models.entities.Class;
import com.fams.fams.models.exception.FamsApiException;
import com.fams.fams.repositories.ClassRepository;
import com.fams.fams.repositories.StudentClassRepository;
import com.fams.fams.services.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ClassServiceImpl implements ClassService {
    private final StudentClassRepository studentClassRepository;
    private final ClassRepository classRepository;

    @Autowired
    public ClassServiceImpl(StudentClassRepository studentClassRepository, ClassRepository classRepository) {
        this.studentClassRepository = studentClassRepository;
        this.classRepository = classRepository;
    }

    @Override
    public Page<Class> getClassesByStudentId(Long studentId, int page, int size) {
        PageRequest pageable = PageRequest.of(page, size);
        Page<Class> classes = studentClassRepository.getClassesByStudentId(studentId, pageable);
        if(classes.isEmpty()){
            throw new FamsApiException(HttpStatus.NOT_FOUND, "Classes not found!");
        }else {
            return classes;
        }
    }

    @Override
    public Page<Class> getAllClass(int page, int size) {
        PageRequest pageable = PageRequest.of(page, size);
        Page<Class> classes = classRepository.findAll(pageable);
        if(classes.isEmpty()){
            throw new FamsApiException(HttpStatus.NOT_FOUND, "Classes not found!");
        }else {
            return classes;
        }
    }


}
