package com.fams.fams.services;

import com.fams.fams.FamsApplication;
import com.fams.fams.models.entities.EmailTemplate;
import com.fams.fams.models.exception.FamsApiException;

import com.fams.fams.models.payload.dto.EmailTemplateDto;
import com.fams.fams.models.payload.requestModel.TemplateEmailAddNew;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


public interface EmailTemplateService {
//    void createTemplate(TemplateEmailAddNew templateEmailAddNew);
//
//    void updateTemplate(long templateId, EmailTemplateDto emailTemplateDto);
//
//    ResponseEntity<FamsApiException> updateTemplateV1(long templateId, EmailTemplateDto emailTemplateDto);
//
//    ResponseEntity<FamsApiException> createTemplateV1(TemplateEmailAddNew templateEmailAddNew);
//    EmailTemplate getTemplateDetails(Long id);
//    ResponseEntity<FamsApiException> createTemplate(TemplateEmailPayload templateEmailPayload);
    Page<EmailTemplateDto> getAllEmailTemplate(int page, int size);
}
